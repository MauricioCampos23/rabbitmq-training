
create schema if not exists training;

SET search_path TO training;

ALTER ROLE postgres SET search_path = training;

CREATE SEQUENCE reminderseq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

create table "Reminder" (
	"Id" int4 not null default nextval('reminderseq'),
	"Date" timestamp not null default CURRENT_TIMESTAMP,
	"Message" varchar(50) not null,
	CONSTRAINT "ReminderPK" PRIMARY KEY ("Id")
);