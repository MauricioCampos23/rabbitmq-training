package com.etc.training.service.interfaces;

public interface IReminderService {
    void sendReminder(Integer id);
}
