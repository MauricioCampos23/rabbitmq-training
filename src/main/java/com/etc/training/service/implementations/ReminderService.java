package com.etc.training.service.implementations;

import com.etc.training.model.Reminder;
import com.etc.training.repository.IReminderRepository;
import com.etc.training.service.interfaces.IReminderService;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Qualifier("reminderService")
@Slf4j
@Service
public class ReminderService implements IReminderService {

    @ConfigProperty(name = "reminder.message_to", defaultValue = "")
    private String MESSAGE_TO;

    private final IReminderRepository reminderRepository;
    private final Mailer mailer;

    public ReminderService(IReminderRepository reminderRepository,
                           Mailer mailer) {
        this.reminderRepository = reminderRepository;
        this.mailer = mailer;
    }

    @Override
    public void sendReminder(Integer id) {
        try {
            log.info("Begin send reminders (SERVICE)");

            Reminder reminder = reminderRepository.findById(id).orElseThrow(()
                    -> new Exception(String.format("No se encontró el recordatorio %s", id.toString())));

            mailer.send(Mail.withText(MESSAGE_TO, "Recordatorio", reminder.getMessage()));

        } catch (Exception e) {
            log.error("ERROR: {}", e.getMessage());
        }

    }
}
