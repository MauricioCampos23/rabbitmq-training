package com.etc.training.rabbitmq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import io.quarkus.runtime.StartupEvent;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@Slf4j
@ApplicationScoped
public class RabbitMqConfig {

    @Value("${app.mq.rabbitmqclient.username}")
    String username;

    @Value("${app.mq.rabbitmqclient.password}")
    String password;

    @Value("${app.mq.rabbitmqclient.virtual-host}")
    String virtualHost;

    @Value("${app.mq.rabbitmqclient.hostname}")
    String hostname;

    @Value("${app.mq.rabbitmqclient.port}")
    int portNumber;

    @Getter
    private Connection connection;

    public void onApplicationStart(@Observes StartupEvent event) {
        setupQueues();
    }

    private void setupQueues() {
        try {
            log.info("Starting connection with RabbitMQ [host={}:{}]", hostname, portNumber);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername(username);
            factory.setPassword(password);
            factory.setVirtualHost(virtualHost);
            factory.setHost(hostname);
            factory.setPort(portNumber);

            connection = factory.newConnection();

        } catch (Exception e) {
            log.error("Error connection RabbitMQ: {}", e.getMessage());
        }
    }

}
