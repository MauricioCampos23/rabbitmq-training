package com.etc.training.rabbitmq;

import com.etc.training.dto.ReminderDto;
import com.etc.training.service.interfaces.IReminderService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.enterprise.context.control.ActivateRequestContext;

@Qualifier("reminderServiceMq")
@Slf4j
@Component
@ActivateRequestContext
public class ReminderServiceMq extends BaseSendMessageMq implements ISendMessageMq {

    @Autowired
    private IReminderService reminderService;

    @Override
    protected String queueName() {
        return "REMINDER_MQ_QUEUE";
    }

    @Override
    protected void received(String message) {

        try {
            log.info("Start to send reminders (RABBITMQ)");
            log.info("Message: [{}]", message);

            if(StringUtils.isNoneBlank(message)) {
                ReminderDto reminder = new Gson().fromJson(message, ReminderDto.class);
                reminderService.sendReminder(reminder.getId());
            }

        } catch (Exception e) {
            log.error("ERROR: {}", e.getMessage());
        }

    }
}
