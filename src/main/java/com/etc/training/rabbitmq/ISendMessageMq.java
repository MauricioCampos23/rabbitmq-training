package com.etc.training.rabbitmq;

public interface ISendMessageMq {
    void send(Object object);
}
