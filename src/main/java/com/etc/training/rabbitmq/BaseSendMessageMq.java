package com.etc.training.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public abstract class BaseSendMessageMq implements ISendMessageMq {

    @Inject
    RabbitMqConfig rabbitMqConfig;

    protected Channel channel;

    protected abstract String queueName();

    protected abstract void received(String message);

    protected String exchangeName() {
        return queueName().concat("_EXCHANGE");
    }

    protected String routingKey() {
        return queueName().concat("_ROUTING");
    }

    protected String consumerTag() {
        return queueName().concat("_CONSUMER");
    }

    private Channel getChannel() {
        try {
            Connection connection = rabbitMqConfig.getConnection();

            if (connection != null && connection.isOpen()) {
                if (channel == null || !channel.isOpen()) {
                    channel = connection.createChannel();
                    channel.exchangeDeclare(exchangeName(), "direct", true);
                    channel.queueDeclare(queueName(), true, false, false, null);
                    channel.queueBind(queueName(), exchangeName(), routingKey());

                    setupReceiving(channel);
                }
            } else {
                log.error("Error connection RabbitMQ is closed");
            }

        } catch (Exception e) {
            log.error("Error connection RabbitMQ: {}", e.getMessage());
        }

        if (channel.isOpen()) {
            return channel;
        }

        throw new RuntimeException("Channel RabbitMQ is closed");
    }

    private void setupReceiving(Channel channel) {
        try {
            if (channel.isOpen()) {
                channel.basicConsume(queueName(), false, consumerTag(),
                        new DefaultConsumer(channel) {
                            @Override
                            public void handleDelivery(String consumerTag,
                                                       Envelope envelope,
                                                       AMQP.BasicProperties properties,
                                                       byte[] body) throws IOException {

                                String routingKey = envelope.getRoutingKey();
                                String contentType = properties.getContentType();
                                long deliveryTag = envelope.getDeliveryTag();
                                channel.basicAck(deliveryTag, false);
                                String message = new String(body, StandardCharsets.UTF_8);
                                // log.info("Received [routingKey={}]: {}", routingKey, message);
                                received(message);
                            }
                        });
            } else {
                log.warn("Channel [{}] is closed", exchangeName());
            }


        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void send(Object obj) {
        try {
            String message = new Gson().toJson(obj);
            log.info("Sending: {}", message);
            byte[] messageBodyBytes = message.getBytes(StandardCharsets.UTF_8);

            getChannel().basicPublish(
                    exchangeName(),
                    routingKey(),
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    messageBodyBytes);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
