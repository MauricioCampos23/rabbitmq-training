package com.etc.training.controller;

import com.etc.training.dto.ReminderQueueResponse;
import com.etc.training.queue.interfaces.IReminderQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(
        path = "api/v1/reminder",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ReminderController {

    private final IReminderQueue reminderQueue;

    public ReminderController(IReminderQueue reminderQueue) {
        this.reminderQueue = reminderQueue;
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping
    public @ResponseBody
    ReminderQueueResponse sendReminders() {
        log.info("Start to send reminders (CONTROLLER)");
        return reminderQueue.sendRemindersQueue();
    }

}
