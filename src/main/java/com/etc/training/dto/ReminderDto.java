package com.etc.training.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RegisterForReflection
public class ReminderDto implements Serializable {

    private Integer id;

    public ReminderDto(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ReminderDto{" +
                "id=" + id +
                '}';
    }
}
