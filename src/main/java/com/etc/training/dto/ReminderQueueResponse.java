package com.etc.training.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@RegisterForReflection
public class ReminderQueueResponse implements Serializable {
    private String message;
    private Integer size = 0;

    @Override
    public String toString() {
        return "ReminderQueueResponse{" +
                "message='" + message + '\'' +
                ", size=" + size +
                '}';
    }
}
