package com.etc.training.repository;

import com.etc.training.model.Reminder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IReminderRepository extends JpaRepository<Reminder, Integer> {
    List<Reminder> findByDateGreaterThanEqualAndDateLessThanEqual(Date dateIni, Date dateFin);
}
