package com.etc.training.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

@Getter
@Setter
@Entity
@Table(name = "`Reminder`")
public class Reminder implements Serializable {

    @Id
    @SequenceGenerator(name = "`reminderseq`", sequenceName = "`reminderseq`", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "`reminderseq`")
    @Column(name = "`Id`", nullable = false)
    private Integer id;

    @Temporal(TIMESTAMP)
    @Column(name = "`Date`", nullable = false)
    private Date date;
    @Column(name = "`Message`", nullable = false)
    private String message;
}
