package com.etc.training.queue.implentations;

import com.etc.training.dto.ReminderDto;
import com.etc.training.dto.ReminderQueueResponse;
import com.etc.training.model.Reminder;
import com.etc.training.queue.interfaces.IReminderQueue;
import com.etc.training.rabbitmq.ISendMessageMq;
import com.etc.training.repository.IReminderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Qualifier("reminderQueue")
@Slf4j
@Component
public class ReminderQueue implements IReminderQueue {

    private final ISendMessageMq sendMessageMq;
    private final IReminderRepository reminderRepository;

    public ReminderQueue(@Qualifier("reminderServiceMq") ISendMessageMq sendMessageMq,
                         IReminderRepository reminderRepository) {
        this.sendMessageMq = sendMessageMq;
        this.reminderRepository = reminderRepository;
    }

    @Override
    public ReminderQueueResponse sendRemindersQueue() {
        log.info("Begin queue reminders (QUEUE)");

        ReminderQueueResponse response = new ReminderQueueResponse();

        Calendar calendar = Calendar.getInstance();

        Date dateIni = new Date();
        dateIni.setHours(0);
        dateIni.setMinutes(0);
        dateIni.setSeconds(0);

        calendar.setTime(dateIni);
        calendar.add(Calendar.DATE, 1);

        Date dateFin = calendar.getTime();


        List<Reminder> reminderList = reminderRepository.findByDateGreaterThanEqualAndDateLessThanEqual(dateIni, dateFin);

        for(Reminder reminder: reminderList) {
            sendMessageMq.send(new ReminderDto(reminder.getId()));
        }

        response.setMessage("Proceso de recordatorios iniciado");
        response.setSize(reminderList.size());

        return response;
    }
}
