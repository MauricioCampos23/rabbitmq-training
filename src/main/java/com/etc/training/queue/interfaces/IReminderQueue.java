package com.etc.training.queue.interfaces;

import com.etc.training.dto.ReminderQueueResponse;

public interface IReminderQueue {
    ReminderQueueResponse sendRemindersQueue();
}
